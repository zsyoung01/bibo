package com.it.bibo.controller.user;


import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("user")
public class UserController {

    //日志
    private final Log log= LogFactory.getLog(UserController.class);

    /**
     * 注册
     * @return
     */
    @RequestMapping("/register")
    @ResponseBody
    public Object register(){
        return true;
    }

    /**
     * 登录
     * @return
     */
    @RequestMapping("/login")
    @ResponseBody
    public Object login(){
        return true;
    }


    /**
     * 退出登录
     * @return
     */
    @RequestMapping("/logout")
    @ResponseBody
    public Object logout(){
        return true;
    }

    /**
     * 清除个人缓存
     * @return
     */
    @RequestMapping("/clearCache")
    @ResponseBody
    public Object clearCache(){
        return true;
    }

    /**
     * 用户身份识别
     * @return
     */
    @RequestMapping("/identification")
    @ResponseBody
    public Object  identification(){
        return true;
    }


}
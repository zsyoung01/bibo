package com.it.bibo.controller.user;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("balance")
public class BalanceController {
    //日志
    private final Log log= LogFactory.getLog(BalanceController.class);

    /**
     * 余额查询
     * @return
     */
    @RequestMapping("/queryBalance")
    @ResponseBody
    public Object queryBalance(){
        return true;
    }
}

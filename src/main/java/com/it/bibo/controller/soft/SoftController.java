package com.it.bibo.controller.soft;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("soft")
public class SoftController {
    //日志
    private final Log log= LogFactory.getLog(SoftController.class);

    /**
     * 添加软件公告
     * @return
     */
    @RequestMapping("/addNotice")
    @ResponseBody
    public Object addNotice(){
        return true;
    }

    /**
     * 删除软件公告
     * @return
     */
    @RequestMapping("/deleteNotice")
    @ResponseBody
    public Object deleteNotice(){
        return true;
    }

    /**
     * 读取软件公告
     * @return
     */
    @RequestMapping("/readNotice")
    @ResponseBody
    public Object readNotice(){
        return true;
    }

    /**
     * 联系我们
     * @return
     */
    @RequestMapping("/contactUs")
    @ResponseBody
    public Object contactUs(){
        return true;
    }

    /**
     * 反馈建议
     * @return
     */
    @RequestMapping("/feedback")
    @ResponseBody
    public Object feedback(){
        return true;
    }

    /**
     * 检查更新
     * @return
     */
    @RequestMapping("/checkUpdate")
    @ResponseBody
    public Object checkUpdate(){
        return true;
    }
}

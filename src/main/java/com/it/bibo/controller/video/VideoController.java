package com.it.bibo.controller.video;


import com.it.bibo.Service.video.api.VideoService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("video")
public class VideoController {
    //日志
    private final Log log= LogFactory.getLog(VideoController.class);
    private VideoService videoService;

    /**
     * 添加视频
     * @return
     */
    @RequestMapping("/addVideo")
    @ResponseBody
    public void addVideo(){
        this.videoService.addVideo();
    }

    /**
     * 删除视频
     * @return
     */
    @RequestMapping("/deleteVideo")
    @ResponseBody
    public Object deleteVideo(@RequestParam String  videoId){
       return this.videoService.deleteVideo(videoId);
    }
    /**
     * 更新视频
     * @return
     */
    @RequestMapping("/updateVideo")
    @ResponseBody
    public Object updateVideo(@RequestParam String  videoId){
        return this.videoService.updateVideo(videoId);
    }

    /**
     * 读取所有视频
     * @return
     */
    @RequestMapping("/readAllVideo")
    @ResponseBody
    public Object readAllVideo(){
        return this.videoService.readAllVideo();
    }

    /**
     * 根据ID读取视频
     * @return
     */
    @RequestMapping("/readVideoById")
    @ResponseBody
    public Object readVideoById(@RequestParam String videoId){
        return this.videoService.readVideoById(videoId);
    }

    /**
     * 根据分类读取视频
     * @return
     */
    @RequestMapping("/readVideoByCategory")
    @ResponseBody
    public Object readVideoByCategory(@RequestParam String videoCategory){
        return this.videoService.readVideoByCategory(videoCategory);
    }
    /**
     * 根据ID读取视频截图
     * @return
     */
    @RequestMapping("/readThumbnailById")
    @ResponseBody
    public Object readThumbnailById(@RequestParam String videoId){
        return this.videoService.readThumbnailById(videoId);
    }

    /**
     * 收藏视频
     * @return
     */
    @RequestMapping("/collectVideo")
    @ResponseBody
    public Object collectVideo(@RequestParam String videoId){
        return this.videoService.collectVideo(videoId);
    }

    /**
     * 分享视频
     * @return
     */
    @RequestMapping("/Video")
    @ResponseBody
    public Object shareVideo(@RequestParam String videoId){
        return this.videoService.shareVideo(videoId);
    }
}

package com.it.bibo.Service.video.api;

public interface VideoService {
    Object addVideo();

    Object deleteVideo(String videoId);

    Object updateVideo(String videoId);

    Object readAllVideo();

    Object readVideoById(String videoId);

    Object readVideoByCategory(String videoCategory);

    Object readThumbnailById(String videoId);

    Object collectVideo(String videoId);

    Object shareVideo(String videoId);
}

package com.it.bibo.Service.video.impl;

import com.it.bibo.Service.video.api.VideoService;

public class IVideoServiceImpl implements VideoService {
    public Object addVideo() {
        return null;
    }

    public Object deleteVideo(String videoId) {
        return null;
    }

    public Object updateVideo(String videoId) {
        return null;
    }

    public Object readAllVideo() {
        return null;
    }

    public Object readVideoById(String videoId) {
        return null;
    }

    public Object readVideoByCategory(String videoCategory) {
        return null;
    }

    public Object readThumbnailById(String videoId) {
        return null;
    }

    public Object collectVideo(String videoId) {
        return null;
    }

    public Object shareVideo(String videoId) {
        return null;
    }
}

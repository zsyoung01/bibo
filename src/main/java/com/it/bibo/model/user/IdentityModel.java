package com.it.bibo.model.user;

import javax.persistence.*;

@Entity
@Table(name = "identity", schema = "bibo", catalog = "")
public class IdentityModel {
    private int identityId;
    private String identityName;
    private String remark;

    @Id
    @Column(name = "identity_id")
    public int getIdentityId() {
        return identityId;
    }

    public void setIdentityId(int identityId) {
        this.identityId = identityId;
    }

    @Basic
    @Column(name = "identity_name")
    public String getIdentityName() {
        return identityName;
    }

    public void setIdentityName(String identityName) {
        this.identityName = identityName;
    }

    @Basic
    @Column(name = "remark")
    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        IdentityModel that = (IdentityModel) o;

        if (identityId != that.identityId) return false;
        if (identityName != null ? !identityName.equals(that.identityName) : that.identityName != null) return false;
        if (remark != null ? !remark.equals(that.remark) : that.remark != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = identityId;
        result = 31 * result + (identityName != null ? identityName.hashCode() : 0);
        result = 31 * result + (remark != null ? remark.hashCode() : 0);
        return result;
    }
}

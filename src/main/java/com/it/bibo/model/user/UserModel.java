package com.it.bibo.model.user;

import javax.persistence.*;

@Entity
@Table(name = "user", schema = "bibo", catalog = "")
public class UserModel {
    private String userId;
    private String userName;
    private String password;
    private String nickname;
    private String promoCode;
    private String parentPromoCode;
    private String deviceCode;
    private Integer identityId;
    private Double balance;

    @Id
    @Column(name = "user_id")
    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Basic
    @Column(name = "user_name")
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @Basic
    @Column(name = "password")
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Basic
    @Column(name = "nickname")
    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    @Basic
    @Column(name = "promo_code")
    public String getPromoCode() {
        return promoCode;
    }

    public void setPromoCode(String promoCode) {
        this.promoCode = promoCode;
    }

    @Basic
    @Column(name = "parent_promo_code")
    public String getParentPromoCode() {
        return parentPromoCode;
    }

    public void setParentPromoCode(String parentPromoCode) {
        this.parentPromoCode = parentPromoCode;
    }

    @Basic
    @Column(name = "device_code")
    public String getDeviceCode() {
        return deviceCode;
    }

    public void setDeviceCode(String deviceCode) {
        this.deviceCode = deviceCode;
    }

    @Basic
    @Column(name = "identity_id")
    public Integer getIdentityId() {
        return identityId;
    }

    public void setIdentityId(Integer identityId) {
        this.identityId = identityId;
    }

    @Basic
    @Column(name = "balance")
    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserModel userModel = (UserModel) o;

        if (userId != null ? !userId.equals(userModel.userId) : userModel.userId != null) return false;
        if (userName != null ? !userName.equals(userModel.userName) : userModel.userName != null) return false;
        if (password != null ? !password.equals(userModel.password) : userModel.password != null) return false;
        if (nickname != null ? !nickname.equals(userModel.nickname) : userModel.nickname != null) return false;
        if (promoCode != null ? !promoCode.equals(userModel.promoCode) : userModel.promoCode != null) return false;
        if (parentPromoCode != null ? !parentPromoCode.equals(userModel.parentPromoCode) : userModel.parentPromoCode != null)
            return false;
        if (deviceCode != null ? !deviceCode.equals(userModel.deviceCode) : userModel.deviceCode != null) return false;
        if (identityId != null ? !identityId.equals(userModel.identityId) : userModel.identityId != null) return false;
        if (balance != null ? !balance.equals(userModel.balance) : userModel.balance != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = userId != null ? userId.hashCode() : 0;
        result = 31 * result + (userName != null ? userName.hashCode() : 0);
        result = 31 * result + (password != null ? password.hashCode() : 0);
        result = 31 * result + (nickname != null ? nickname.hashCode() : 0);
        result = 31 * result + (promoCode != null ? promoCode.hashCode() : 0);
        result = 31 * result + (parentPromoCode != null ? parentPromoCode.hashCode() : 0);
        result = 31 * result + (deviceCode != null ? deviceCode.hashCode() : 0);
        result = 31 * result + (identityId != null ? identityId.hashCode() : 0);
        result = 31 * result + (balance != null ? balance.hashCode() : 0);
        return result;
    }
}

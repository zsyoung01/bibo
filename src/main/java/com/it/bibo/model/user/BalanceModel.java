package com.it.bibo.model.user;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "balance", schema = "bibo", catalog = "")
public class BalanceModel {
    private String balanceId;
    private Double accountBalance;
    private Timestamp rechargeTime;
    private Timestamp consumeTime;

    @Id
    @Column(name = "balance_id")
    public String getBalanceId() {
        return balanceId;
    }

    public void setBalanceId(String balanceId) {
        this.balanceId = balanceId;
    }

    @Basic
    @Column(name = "account_balance")
    public Double getAccountBalance() {
        return accountBalance;
    }

    public void setAccountBalance(Double accountBalance) {
        this.accountBalance = accountBalance;
    }

    @Basic
    @Column(name = "recharge_time")
    public Timestamp getRechargeTime() {
        return rechargeTime;
    }

    public void setRechargeTime(Timestamp rechargeTime) {
        this.rechargeTime = rechargeTime;
    }

    @Basic
    @Column(name = "consume_time")
    public Timestamp getConsumeTime() {
        return consumeTime;
    }

    public void setConsumeTime(Timestamp consumeTime) {
        this.consumeTime = consumeTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BalanceModel that = (BalanceModel) o;

        if (balanceId != null ? !balanceId.equals(that.balanceId) : that.balanceId != null) return false;
        if (accountBalance != null ? !accountBalance.equals(that.accountBalance) : that.accountBalance != null)
            return false;
        if (rechargeTime != null ? !rechargeTime.equals(that.rechargeTime) : that.rechargeTime != null) return false;
        if (consumeTime != null ? !consumeTime.equals(that.consumeTime) : that.consumeTime != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = balanceId != null ? balanceId.hashCode() : 0;
        result = 31 * result + (accountBalance != null ? accountBalance.hashCode() : 0);
        result = 31 * result + (rechargeTime != null ? rechargeTime.hashCode() : 0);
        result = 31 * result + (consumeTime != null ? consumeTime.hashCode() : 0);
        return result;
    }
}

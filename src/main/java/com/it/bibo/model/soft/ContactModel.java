package com.it.bibo.model.soft;

import javax.persistence.*;

@Entity
@Table(name = "contact", schema = "bibo", catalog = "")
public class ContactModel {
    private String contactId;
    private String qqGroup;
    private String qqPersonal;

    @Id
    @Column(name = "contact_id")
    public String getContactId() {
        return contactId;
    }

    public void setContactId(String contactId) {
        this.contactId = contactId;
    }

    @Basic
    @Column(name = "qq_group")
    public String getQqGroup() {
        return qqGroup;
    }

    public void setQqGroup(String qqGroup) {
        this.qqGroup = qqGroup;
    }

    @Basic
    @Column(name = "qq_personal")
    public String getQqPersonal() {
        return qqPersonal;
    }

    public void setQqPersonal(String qqPersonal) {
        this.qqPersonal = qqPersonal;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ContactModel that = (ContactModel) o;

        if (contactId != null ? !contactId.equals(that.contactId) : that.contactId != null) return false;
        if (qqGroup != null ? !qqGroup.equals(that.qqGroup) : that.qqGroup != null) return false;
        if (qqPersonal != null ? !qqPersonal.equals(that.qqPersonal) : that.qqPersonal != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = contactId != null ? contactId.hashCode() : 0;
        result = 31 * result + (qqGroup != null ? qqGroup.hashCode() : 0);
        result = 31 * result + (qqPersonal != null ? qqPersonal.hashCode() : 0);
        return result;
    }
}

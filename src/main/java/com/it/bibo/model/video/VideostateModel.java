package com.it.bibo.model.video;

import javax.persistence.*;

@Entity
@Table(name = "videostate", schema = "bibo", catalog = "")
public class VideostateModel {
    private int videoStateId;
    private String name;
    private Integer order;
    private String cssstyle;
    private String remark;

    @Id
    @Column(name = "video_state_id")
    public int getVideoStateId() {
        return videoStateId;
    }

    public void setVideoStateId(int videoStateId) {
        this.videoStateId = videoStateId;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "order")
    public Integer getOrder() {
        return order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }

    @Basic
    @Column(name = "cssstyle")
    public String getCssstyle() {
        return cssstyle;
    }

    public void setCssstyle(String cssstyle) {
        this.cssstyle = cssstyle;
    }

    @Basic
    @Column(name = "remark")
    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        VideostateModel that = (VideostateModel) o;

        if (videoStateId != that.videoStateId) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (order != null ? !order.equals(that.order) : that.order != null) return false;
        if (cssstyle != null ? !cssstyle.equals(that.cssstyle) : that.cssstyle != null) return false;
        if (remark != null ? !remark.equals(that.remark) : that.remark != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = videoStateId;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (order != null ? order.hashCode() : 0);
        result = 31 * result + (cssstyle != null ? cssstyle.hashCode() : 0);
        result = 31 * result + (remark != null ? remark.hashCode() : 0);
        return result;
    }
}

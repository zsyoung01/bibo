package com.it.bibo.model.video;

import javax.persistence.*;

@Entity
@Table(name = "configure", schema = "bibo", catalog = "")
public class ConfigureModel {
    private int configureId;
    private String name;
    private String val;
    private String remark;

    @Id
    @Column(name = "configure_id")
    public int getConfigureId() {
        return configureId;
    }

    public void setConfigureId(int configureId) {
        this.configureId = configureId;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "val")
    public String getVal() {
        return val;
    }

    public void setVal(String val) {
        this.val = val;
    }

    @Basic
    @Column(name = "remark")
    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ConfigureModel that = (ConfigureModel) o;

        if (configureId != that.configureId) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (val != null ? !val.equals(that.val) : that.val != null) return false;
        if (remark != null ? !remark.equals(that.remark) : that.remark != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = configureId;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (val != null ? val.hashCode() : 0);
        result = 31 * result + (remark != null ? remark.hashCode() : 0);
        return result;
    }
}
